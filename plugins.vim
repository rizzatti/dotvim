"dein Scripts-----------------------------
if &compatible
  set nocompatible               " Be iMproved
endif

" Required:
set runtimepath+=/Users/joseferreira/.cache/dein/repos/github.com/Shougo/dein.vim

" Required:
if dein#load_state('/Users/joseferreira/.cache/dein')
  call dein#begin('/Users/joseferreira/.cache/dein')

  " Let dein manage dein
  " Required:
  call dein#add('/Users/joseferreira/.cache/dein/repos/github.com/Shougo/dein.vim')

  call dein#add('editorconfig/editorconfig-vim')

  " Filetypes
  call dein#add('vim-scripts/matchit.zip')

  call dein#add('sheerun/vim-polyglot')

  call dein#add('tpope/vim-rbenv', { 'on_ft': 'ruby' })
  call dein#add('tpope/vim-bundler', { 'on_ft': 'ruby' })
  call dein#add('tpope/vim-rails', { 'on_ft': 'ruby' })

  call dein#add('tmhedberg/SimpylFold', { 'on_ft': 'python' })

  call dein#add('Chiel92/vim-autoformat')

  " Utils
  " call dein#add('neomake/neomake')
  " call dein#add('c0r73x/neotags.nvim')
  " call dein#add('janko-m/vim-test')
  " call dein#add('eugen0329/vim-esearch')

  call dein#add('Valloric/YouCompleteMe', {
        \ 'build': './install.py'
        \ })

  call dein#add('SirVer/ultisnips')

  call dein#add('honza/vim-snippets')

  call dein#add('Shougo/vimproc.vim', {'build': 'make'})

  " call dein#add('Shougo/denite.nvim')

  call dein#add('tpope/vim-fugitive')

  call dein#add('gregsexton/gitv')

  call dein#add('valloric/MatchTagAlways')

  call dein#add('sickill/vim-pasta')

  call dein#add('tpope/vim-commentary')

  call dein#add('tpope/vim-eunuch')

  call dein#add('tpope/vim-repeat')

  call dein#add('tpope/vim-surround')

  call dein#add('tpope/vim-characterize')

  call dein#add('tpope/vim-speeddating')

  call dein#add('godlygeek/tabular')

  call dein#add('rizzatti/dash.vim')

  call dein#add('mhinz/vim-grepper')

  " UI
  call dein#add('morhetz/gruvbox')

  call dein#add('bling/vim-airline')

  " Utils
  call dein#add('scrooloose/nerdtree')

  " Required:
  call dein#end()
  call dein#save_state()
endif

" Required:
filetype plugin indent on
syntax enable

"End dein Scripts-------------------------

let g:SimpylFold_docstring_preview = 1
let g:SimpylFold_fold_import = 0
autocmd BufWinEnter *.py setlocal foldexpr=SimpylFold(v:lnum) foldmethod=expr
autocmd BufWinLeave *.py setlocal foldexpr< foldmethod<

let g:formatdef_yapf = "'yapf -l '.a:firstline.'-'.a:lastline"

let g:ycm_complete_in_comments_and_strings=0
let g:ycm_key_list_select_completion=['<C-n>', '<Down>']
let g:ycm_key_list_previous_completion=['<C-p>', '<Up>']
let g:ycm_filetype_blacklist={'unite': 1}
let g:ycm_add_preview_to_completeopt=0
let g:ycm_autoclose_preview_window_after_completion=1
let g:ycm_collect_identifiers_from_tags_files=1
let g:ycm_key_detailed_diagnostics='<Leader>x'
let g:ycm_confirm_extra_conf=1
let g:ycm_extra_conf_globlist=['~/code/*','!~/*']
let g:ycm_server_log_level='debug'
let g:ycm_server_keep_logfiles=1
" let g:ycm_python_binary_path='python'
" let g:ycm_server_use_vim_stdout=1
" let g:ycm_path_to_python_interpreter=$HOME . '/.pyenv/shims/python'

let g:UltiSnipsExpandTrigger="<tab>"
let g:UltiSnipsJumpForwardTrigger="<tab>"
let g:UltiSnipsJumpBackwardTrigger="<s-tab>"
let g:UltiSnipsSnippetsDir='~/.vim/snippets'

nmap <silent> <Leader>d <Plug>DashSearch
vmap <silent> <Leader>d <Plug>DashSearch
nmap <silent> <Leader>D <Plug>DashGlobalSearch
vmap <silent> <Leader>D <Plug>DashGlobalSearch

nnoremap <silent> <C-t> :NERDTreeToggle<CR>
autocmd FileType nerdtree setlocal nolist

set background=dark
colorscheme gruvbox
set hidden
let g:airline#extensions#tabline#enabled = 1
let g:airline_powerline_fonts = 1
