if has("gui_macvim")
  set number
  set lines=25
  set columns=80
  set guifont=Ubuntu\ Mono\ derivative\ Powerline\ Nerd\ Font\ Complete\ Mono:h14
  " set guifont=Sauce\ Code\ Pro\ Medium\ Nerd\ Font\ Complete:h12
  set guioptions-=r
  set guioptions-=R
  set guioptions-=l
  set guioptions-=L
  set bg=dark
  nnoremap <D-0> 0gt
  nnoremap <D-1> 1gt
  nnoremap <D-2> 2gt
  nnoremap <D-3> 3gt
  nnoremap <D-4> 4gt
  nnoremap <D-5> 5gt
  nnoremap <D-6> 6gt
  nnoremap <D-7> 7gt
  nnoremap <D-8> 8gt
  nnoremap <D-9> 9gt
  macmenu Window.Toggle\ Full\ Screen\ Mode key=<D-CR>
endif
