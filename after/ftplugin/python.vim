setlocal foldmethod=indent
setlocal textwidth=119

nnoremap <F11> :!ctags -R -f tags . $VIRTUAL_ENV/lib/python2.7/site-packages/<CR>
