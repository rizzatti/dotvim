"{{{ General
set nocompatible
set ignorecase
set smartcase
set gdefault
set hidden
set nopaste
set matchtime=2
set matchpairs+=<:>
set timeout
set confirm
set lazyredraw
set shell=bash
set noshelltemp
set modeline
set cpoptions+=$
if exists('$TMUX')
  set clipboard=
else
  set clipboard+=unnamed
endif
set pastetoggle=<F2>
set exrc
set secure
set ttyfast
let g:python_host_prog = '/usr/local/bin/python'
let g:python3_host_prog = '/usr/local/bin/python3'
"}}}

"{{{ Navigation
set nostartofline
set noequalalways
set switchbuf=useopen,usetab
set virtualedit=block
"}}}

"{{{ Look
set list
set number
set relativenumber
set noshowmode
set laststatus=2
set nowrap
set wrapscan
set mousehide
set shortmess+=a
set colorcolumn=+1
set completeopt=menu,longest,preview
set foldenable
set scrolloff=5
set sidescrolloff=5
if has('spell')
  set nospell
  set spelllang=en_us
endif
set noerrorbells
set visualbell
set t_vb=
if has('extra_search')
  set hlsearch
endif
if has('linebreak')
  set linebreak
  if &termencoding ==# 'utf-8' || &encoding ==# 'utf-8'
    let &showbreak = "\u21aa"
  endif
endif
if has('title')
  set title
endif
if has('wildmenu')
  set wildmode=longest,list
  set wildignore+=*.a,*.o
  set wildignore+=*.gif,*.ico,*.jpg,*.png
  set wildignore+=.DS_Store,.git,.hg,.svn
  set wildignore+=*~,*.swp,*.tmp
endif
if $TERM_PROGRAM == 'iTerm.app'
  " different cursors for insert vs normal mode
  if exists('$TMUX')
    let &t_SI = "\<Esc>Ptmux;\<Esc>\<Esc>]50;CursorShape=1\x7\<Esc>\\"
    let &t_EI = "\<Esc>Ptmux;\<Esc>\<Esc>]50;CursorShape=0\x7\<Esc>\\"
  else
    let &t_SI = "\<Esc>]50;CursorShape=1\x7"
    let &t_EI = "\<Esc>]50;CursorShape=0\x7"
  endif
endif
"}}}

"{{{ File options
set nobackup
set nowritebackup
set noswapfile
set nofsync
set undolevels=1000
if isdirectory(expand('~/.cache/vim'))
  set directory=~/.cache/vim/swap
  set backupdir=~/.cache/vim/backup
  if exists('+undodir')
    set undodir=~/.cache/vim/undo
  endif
  if exists('+viewdir')
    set viewdir=~/.cache/vim/view
  endif
endif
"}}}

"{{{ Searching
if executable('ack')
  set grepprg=ack\ --nogroup\ --column\ --smart-case\ --nocolor\ --follow\ $*
  set grepformat=%f:%l:%c:%m
endif
if executable('ag')
  set grepprg=ag\ --nogroup\ --column\ --smart-case\ --nocolor\ --follow
  set grepformat=%f:%l:%c:%m
endif
"}}}

let mapleader=','
let maplocalleader='\'

"{{{ Mappings
cnoremap %% <C-r>=expand('%:h').'/'<CR>
nnoremap ; :
vnoremap ; :
nnoremap : ;
vnoremap : ;
nnoremap / /\v
vnoremap / /\v
nnoremap ? ?\v
vnoremap ? ?\v
nnoremap Q gq
vmap < <gv
vmap > >gv
noremap <Down> gj
noremap <Up> gk
nnoremap <silent> <Leader>. :b#<CR>
nnoremap <silent> <Leader>= V`]=
nnoremap <silent> <Leader>cd :lcd %:h<CR>:pwd<CR>
nnoremap <silent> <Leader>md :!mkdir -p %:p:h<CR>
nnoremap <silent> <Leader>s :%s/<C-r><C-w>/
nnoremap <silent> <Leader>th :setlocal hlsearch! hlsearch?<CR>
nnoremap <silent> <Leader>tl :setlocal list! list?<CR>
nnoremap <silent> <Leader>ts :setlocal spell! spell?<CR>
nnoremap <silent> <Leader>tw :setlocal wrap! wrap?<CR>
nnoremap <silent> <Leader>tW :call <SID>ToggleWhitespaceError()<CR>
nnoremap <silent> <Leader>v :tabedit $MYVIMRC<CR>
"}}}

"{{{ Functions
function! s:ToggleWhitespaceError()
  if exists('b:whitespace_error_match') && b:whitespace_error_match
    let b:whitespace_error_match=0
    match none
  else
    let b:whitespace_error_match=1
    match Error /\v\s+$/
  endif
endfunction

function! s:JumpToLastPosition()
  if &filetype ==? 'gitcommit'
    return
  endif
  if line('''"') > 1 && line('''"') <= line('$')
    execute 'normal! g`"'
  endif
endfunction
"}}}

"{{{ Auto commands
if has('autocmd')
  augroup Mappings
    autocmd!
    autocmd BufReadPost $MYVIMRC
          \ nnoremap <buffer> <silent> <LocalLeader>s :source $MYVIMRC<CR>
  augroup END

  augroup Modes
    autocmd!
    autocmd BufEnter * setlocal cursorline
    autocmd BufLeave * setlocal nocursorline
    autocmd InsertEnter * set timeoutlen=0
    autocmd InsertLeave * set timeoutlen=1000
    autocmd InsertEnter * setlocal nolist nocursorline nohlsearch
    autocmd InsertLeave * setlocal list cursorline hlsearch
  augroup END

  augroup Window
    autocmd!
    autocmd BufReadPost * call s:JumpToLastPosition()
    autocmd VimResized * wincmd =
  augroup END
endif
"}}}

if filereadable(expand('.local.vim'))
  source .local.vim
endif

if filereadable(expand('~/.config/nvim/plugins.vim'))
  source ~/.config/nvim/plugins.vim
endif

nohlsearch
